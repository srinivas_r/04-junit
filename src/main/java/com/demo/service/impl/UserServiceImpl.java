package com.demo.service.impl;

import com.demo.dao.UserDAO;
import com.demo.service.UserService;

public class UserServiceImpl implements UserService {
	
	UserDAO  userDao;
	
	public UserServiceImpl(UserDAO userDao) {
		this.userDao = userDao;
	}

	@Override
	public String findNameById(Integer userId) {
		return userDao.fetchNameById(userId);
	}

	@Override
	public String findCityById(Integer userId) {
		return userDao.fetchCityById(userId);
	}
	
	@Override
	public String addReview(Integer userId, String review) {
		userDao.writeReview(userId, review);
		return "success";
	}

}
