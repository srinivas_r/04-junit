package com.demo.dao;

public interface UserDAO {
	
	String fetchNameById(Integer userId);
	String fetchCityById(Integer userId);
	void writeReview(Integer userId, String review);

}
