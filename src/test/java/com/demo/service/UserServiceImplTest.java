package com.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.demo.dao.UserDAO;
import com.demo.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {
	
	@Mock
	UserDAO  userDaoMock;
	
	@InjectMocks
	UserServiceImpl  service;
	
	@Test
	public void findNameByIdTest() {
		
		when(userDaoMock.fetchNameById(101)).thenReturn("Anil");
		String actual = service.findNameById(101);
		assertEquals("Anil", actual);
		
	}
	
	@Test
	public void findCityByIdTest() {
		when(userDaoMock.fetchCityById(101)).thenReturn("Delhi");
		String actual = service.findCityById(101);
		assertEquals("Hyderabad", actual);
	}
	
	@Test
	public void addReviewTest() {
		doNothing().when(userDaoMock).writeReview(anyInt(), anyString());
		String actual = service.addReview(1011, "Good");
		assertEquals("success", actual);
		
	}
	

}
