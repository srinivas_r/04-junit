package com.demo.service;

public interface UserService {
	String findNameById(Integer userId);
	String findCityById(Integer userId);
    String addReview(Integer userId, String review);
}
